package model;

import entities.Barber;
import entities.Service;
import entities.Client;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import entities.Appointment;

public class BarberShop {
    private String name;
    private List<Barber> barbers;
    private List<Service> services;
    private List<Client> clients;
    private List<Appointment> appointments;

    public BarberShop(String name) {
        this.name = name;
        this.barbers = new ArrayList<>();
        this.services = new ArrayList<>();
        this.clients = new ArrayList<>();
        this.appointments = new ArrayList<>();
    }

    public void addBarber(Barber barber) {
        barbers.add(barber);
    }

    public void addService(Service service) {
        services.add(service);
    }

    public void addClient(Client client) {
        clients.add(client);
    }

    public void scheduleAppointment(Client client, Barber barber, Service service, LocalDateTime dateTime) {
        if (!clients.contains(client) || !barbers.contains(barber) || !services.contains(service)) {
            throw new IllegalArgumentException("Cliente, barbeiro ou serviço não encontrado na barbearia.");
        }
        Appointment appointment = new Appointment((long) (appointments.size() + 1), dateTime, client, barber, service);
        appointments.add(appointment);
    }

    public List<Appointment> getAppointments() {
        return appointments;
    }

    public List<Appointment> getAppointmentsByClient(Client client) {
        return appointments.stream()
                .filter(appointment -> appointment.getClient().equals(client))
                .collect(Collectors.toList());
    }

    public List<Appointment> getAppointmentsByBarber(Barber barber) {
        return appointments.stream()
                .filter(appointment -> appointment.getBarber().equals(barber))
                .collect(Collectors.toList());
    }

    public List<Appointment> getAppointmentsByDate(LocalDateTime date) {
        return appointments.stream()
                .filter(appointment -> appointment.getDateTime().toLocalDate().equals(date.toLocalDate()))
                .collect(Collectors.toList());
    }

    @Override
    public String toString() {
        return "BarberShop{" +
                "name='" + name + '\'' +
                ", barbers=" + barbers +
                ", services=" + services +
                ", clients=" + clients +
                ", appointments=" + appointments +
                '}';
    }
}
