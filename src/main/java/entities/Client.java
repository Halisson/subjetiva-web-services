package entities;

public class Client {
    private Long id;
    private String name;
    private String password;

    public Client(Long id, String name, String password) {
        this.id = id;
        this.name = name;
        this.setPassword(password);
    }

    @Override
    public String toString() {
        return "Client{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}

