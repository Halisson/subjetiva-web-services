package entities;

import java.time.LocalDateTime;

public class Appointment {
    private Long id;
    private LocalDateTime dateTime;
    private Client client;
    private Barber barber;
    private Service service;

    public Appointment(Long id, LocalDateTime dateTime, Client client, Barber barber, Service service) {
        this.id = id;
        this.dateTime = dateTime;
        this.client = client;
        this.barber = barber;
        this.service = service;
    }

    public Long getId() {
        return id;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public Client getClient() {
        return client;
    }

    public Barber getBarber() {
        return barber;
    }

    public Service getService() {
        return service;
    }

    @Override
    public String toString() {
        return "Appointment{" +
                "id=" + id +
                ", dateTime=" + dateTime +
                ", client=" + client +
                ", barber=" + barber +
                ", service=" + service +
                '}';
    }
}