package entities;

public class Barber {
    private Long id;
    private String name;

    public Barber(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public String toString() {
        return "Barber{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
