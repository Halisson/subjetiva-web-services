//teste
package principal;

import java.time.LocalDateTime;
import java.util.List;

import entities.Barber;
import entities.Client;
import entities.Service;
import model.BarberShop;
import entities.Appointment;

public class MyApp {
    public static void main(String[] args) {
        // Criando a barbearia
        BarberShop barberShop = new BarberShop("Barbearia do João");

        // Criando alguns barbeiros
        Barber barber1 = new Barber(1L, "João");
        Barber barber2 = new Barber(2L, "Pedro");

        // Adicionando barbeiros à barbearia
        barberShop.addBarber(barber1);
        barberShop.addBarber(barber2);

        // Criando alguns serviços
        Service haircut = new Service(1L, "Corte de Cabelo", 50.0);
        Service shave = new Service(2L, "Barba", 30.0);

        // Adicionando serviços à barbearia
        barberShop.addService(haircut);
        barberShop.addService(shave);

        // Criando alguns clientes
        Client client1 = new Client(1L, "Maria", "senha123");
        Client client2 = new Client(2L, "João", "senha456");

        // Adicionando clientes à barbearia
        barberShop.addClient(client1);
        barberShop.addClient(client2);

        // Agendando alguns serviços para os clientes
        barberShop.scheduleAppointment(client1, barber1, haircut, LocalDateTime.now().plusDays(1));
        barberShop.scheduleAppointment(client2, barber2, shave, LocalDateTime.now().plusDays(2));

        // Exibindo os detalhes da barbearia
        System.out.println(barberShop);

        // Listando todos os agendamentos
        System.out.println("Lista de Agendamentos:");
        List<Appointment> appointments = barberShop.getAppointments();
        appointments.forEach(System.out::println);

        // Listando agendamentos de um cliente específico
        System.out.println("Agendamentos do Cliente 1:");
        List<Appointment> appointmentsOfClient1 = barberShop.getAppointmentsByClient(client1);
        appointmentsOfClient1.forEach(System.out::println);

        // Listando agendamentos de um barbeiro específico
        System.out.println("Agendamentos do Barbeiro 1:");
        List<Appointment> appointmentsOfBarber1 = barberShop.getAppointmentsByBarber(barber1);
        appointmentsOfBarber1.forEach(System.out::println);

        // Listando agendamentos para uma data específica
        LocalDateTime dateToSearch = LocalDateTime.now().plusDays(1);
        System.out.println("Agendamentos para o dia " + dateToSearch.toLocalDate() + ":");
        List<Appointment> appointmentsForDate = barberShop.getAppointmentsByDate(dateToSearch);
        appointmentsForDate.forEach(System.out::println);
    }
}
